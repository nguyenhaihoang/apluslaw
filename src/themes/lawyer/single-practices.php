<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lawyer
 */

get_header();
?>
<?php
while (have_posts()) :
    the_post();
    ?>
    <div class="page-header page-header--thumbnail">
        <?php lawyer_post_thumbnail(); ?>
        <div class="container-layout">
            <?php echo get_hansel_and_gretel_breadcrumbs(); ?>
            <?php
                the_title('<h2 class="page-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
            ?>
            <span class="page-title__sub">
                <?php echo get_the_excerpt(); ?>
            </span>
        </div>
    </div><!-- .page-header -->
    <main id="primary" class="site-page single-content--style">
        <div class="container-layout">
            <div class="ct-row">
                <div class="ct-column-8">
                    <div class="site-page__description">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div class="ct-column-4">
                    <div class="site-page__sidebar">
                        <?php if (get_field('luat_su')) : ?>
                            <div class="single-related">
                                <?php
                                $post_ids = get_field('luat_su');
                                $args = array(
                                    'post_type' => 'professionals',
                                    'post__in' => $post_ids,
                                    'showposts' => -1,
                                );
                                $my_query = new wp_query($args);
                                if ($my_query->have_posts()) {
                                    echo '<h3 class="section__sub-heading">Luật sư phụ trách</h3>';
                                    echo '<div class="ct-row">';
                                    while ($my_query->have_posts()) {
                                        $my_query->the_post();?>
                                        <div class="ct-column-12">
                                        <div class="lawyer-item">
                                            <div class="image">
                                                <a href="#" class="<?php the_permalink(); ?>">
                                                    <?php the_post_thumbnail(); ?>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <?php if (!empty(get_field('info'))) : $info = get_field('info'); if(!empty($info['position'])) : ?>
                                                    <div class="section__sub-heading">
                                                        <span><?php echo $info['position']; ?></span>
                                                    </div>
                                                <?php endif;endif; ?>
                                                <h2 class="title">
                                                    <a href="<?php echo get_permalink(); ?>">
                                                        <?php echo get_the_title(); ?>
                                                    </a>
                                                </h2>
                                                <a href="<?php echo get_permalink(); ?>" class="site-link">
                                                    <?php esc_html_e('Xem chi tiết'); ?>
                                                </a>
                                            </div>
                                        </div>
                                        </div>
                                        <?php
                                    };
                                    echo '</div>';
                                } ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-layout">
            <?php if (get_field('bai_viet_lien_quan')) : ?>
                <div class="single-related">
                    <?php
                    $post_ids = get_field('bai_viet_lien_quan');
                    $args = array(
                        'post_type' => 'post',
                        'post__in' => $post_ids,
                        'showposts' => -1,
                    );
                    $my_query = new wp_query($args);
                    if ($my_query->have_posts()) {
                        echo '<h3 class="section__sub-heading">Bài viết liên quan</h3>';
                        echo '<div class="ct-row ct-row--doubling">';
                        while ($my_query->have_posts()) {
                            $my_query->the_post();
                            echo '<div class="ct-column-3">';
                            get_template_part('template-parts/content', 'grid');
                            echo '</div>';
                        }
                        echo '</div>';
                        wp_reset_query();
                    } ?>
                </div>
            <?php endif; ?>
        </div>
    </main><!-- #main -->
<?php
endwhile; // End of the loop.
?>
<?php
get_footer();
