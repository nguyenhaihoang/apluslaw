<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package lawyer
 */

get_header();
?>
    <header class="page-header">
        <div class="container-layout">
            <h2 class="section__heading section__heading--primary" style="text-align: center;color: #fff;">
                404
            </h2>
            <h1 class="page-title"><?php esc_html_e('Rất tiếc! không thể tim thấy trang đó.', 'lawyer'); ?></h1>
            <div style="text-align: center">
                <a href="<?php echo get_bloginfo('url'); ?>" class="site-link">Quay lại trang chủ</a>
            </div>
        </div>
    </header><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <section class="error-404 not-found">
                <div class="page-content">
                    <p><?php esc_html_e('Có vẻ như không có gì được tìm thấy ở đây. Bạn có thể tìm kiếm mọi thông tin trên Apluslaw tại đây?', 'lawyer'); ?></p>
                    <?php
                    get_search_form();
                    ?>
                </div><!-- .page-content -->
            </section><!-- .error-404 -->
        </div>
    </main><!-- #main -->

<?php
get_footer();
