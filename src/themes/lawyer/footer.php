<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lawyer
 */

?>

<footer id="colophon" class="site-footer">
    <div class="container-layout">
        <div class="site-footer__content">
            <div class="site-footer__row">
                <?php if ( is_active_sidebar( 'footer-widget' ) ) : ?>
                    <?php dynamic_sidebar('footer-widget'); ?>
                <?php endif; ?>
            </div>
        </div>
<!--        <div class="site-info">-->
<!--            <div class="site-footer__row site-footer__row--center">-->
<!--                --><?php //if ( is_active_sidebar( 'footer-widget-copy-right' ) ) : ?>
<!--                    --><?php //dynamic_sidebar('footer-widget-copy-right'); ?>
<!--                --><?php //endif; ?>
<!--            </div>-->
<!--        </div>-->
        <!-- .site-info -->
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
