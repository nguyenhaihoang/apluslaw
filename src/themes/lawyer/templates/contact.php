<?php
/**
 * Template Name: Page layout
 *
 * @package lawyer
 */

get_header();

$excerpt = get_field('mo_ta_ngan');
?>
    <header class="page-header">
        <div class="container-layout">
            <?php
            echo get_hansel_and_gretel_breadcrumbs();
            ?>
            <div class="page-title">
                <?php echo get_the_title(); ?>
            </div>
            <div class="page-description"><span><?php echo $excerpt; ?></span></div>
        </div>
    </header><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <?php
            while ( have_posts() ) :
                the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>
        </div>
    </main><!-- #main -->
<?php
get_footer();
