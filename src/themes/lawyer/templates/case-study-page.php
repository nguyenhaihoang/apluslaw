<?php
/**
 * Template Name: Case study page
 *
 * @package lawyer
 */

get_header();

$services_1 = get_field('service_1');
$services_2 = get_field('service_2');
$services_3 = get_field('service_3');
?>
    <div class="page-header">
        <div class="container-layout">
            <?php
                echo get_hansel_and_gretel_breadcrumbs();
            ?>
            <h1 class="page-title">
                <?php echo get_the_title(); ?>
            </h1>
        </div>
    </div><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <div class="ct-row">
                <div class="ct-column-3">
                    <?php
                    $taxonomies = get_taxonomies(['object_type' => ['case_study']]);
                    $taxonomyTerms = [];
                    // loop over your taxonomies
                    if (!empty($taxonomies)) :
                        foreach ($taxonomies as $taxonomy) {
                            // retrieve all available terms, including those not yet used
                            $terms = get_terms(['taxonomy' => $taxonomy, 'hide_empty' => false]);
                            // make sure $terms is an array, as it can be an int (count) or a WP_Error
                            $hasTerms = is_array($terms) && $terms;

                            if ($hasTerms) {
                                $taxonomyTerms[$taxonomy] = $terms;
                            }
                        } ?>
                        <?php
                        foreach ($taxonomyTerms as $key => $terms) : if ($key === 'case_study_brand' || $key === 'case_study_province') continue; ?>
                            <div class="case-study__sidebar">
                                <h2><?php
                                    switch ($key) {
                                        case 'case_study_category':
                                            echo 'Lĩnh vực';
                                            break;
                                        case 'case_study_year':
                                            echo 'Năm';
                                            break;
                                        case 'case_study_brand':
                                            echo 'Thương hiệu';
                                            break;
                                        case 'case_study_nation':
                                            echo 'Quốc gia';
                                            break;
                                        case 'case_study_province':
                                            echo 'Tỉnh';
                                            break;
                                        default:
                                            echo 'Danh mục';
                                    }
                                    ?>
                                </h2>
                                <ul>
                                    <?php foreach ($terms as $term) :
                                        ?>
                                        <li>
                                            <a href="<?php echo get_term_link($term->term_id) ?>">
                                                <?php echo $term->name; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="ct-column-9">
                    <?php
                    $args = array(
                        'post_type' => 'case_study',
                        'showposts' => -1,
                    );
                    if (!empty($_GET['search'])) {
                        $args['s'] = $_GET['search'];
                    }
                    if (!empty($_GET['order'])) {
                        $args['order'] = $_GET['order'];
                        $args['orderby'] = 'date';
                    }
                    $my_query = new wp_query($args);
                    ?>
                    <div class="case-study__filter">
                        <form method="get" class="filter" action="./">
                            <div class="filter__search">
                                <div class="search-form">
                                    <label>
                                        <span class="screen-reader-text">Tìm kiếm cho:</span>
                                        <input type="search" class="search-field" placeholder="Tìm kiếm …"
                                               value="<?php echo !empty($_GET['search']) ? $_GET['search'] : ''; ?>"
                                               name="search">
                                    </label>
                                    <input type="submit" class="search-submit" value="Tìm kiếm">
                                </div>
                            </div>
                            <div class="filter__order">
                                <select name="order">
                                    <option value="">Xếp theo</option>
                                    <option value="DESC" <?php echo !empty($_GET['order']) && $_GET['order'] === 'DESC' ? 'selected' : ''; ?>>Mới nhất</option>
                                    <option value="ASC" <?php echo !empty($_GET['order']) && $_GET['order'] === 'ASC' ? 'selected' : ''; ?>>Cũ nhất</option>
                                </select>
                            </div>
                        </form>
                        <div class="count">
                            <a href="#"><?php echo $my_query->post_count; ?> bài viết</a>
                        </div>
                    </div>
                    <div class="case-study__posts">
                        <?php
                        if ($my_query->have_posts()) {
                            echo '<div class="ct-row ct-row--col-three ct-row--doubling">';
                            while ($my_query->have_posts()) {
                                $my_query->the_post();
                                echo '<div class="ct-column">';
                                get_template_part('template-parts/content', 'grid-case-study');
                                echo '</div>';
                            }
                            echo '</div>';
                            wp_reset_query();
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <section class="case-study__services">
            <div class="container-layout">
                <h2 class="s-heading">
                    A+ có thể giúp được gì cho bạn?
                </h2>
                <div class="ct-row">
                    <?php if (!empty($services_1)) : ?>
                        <div class="ct-column-4">
                            <div class="case-study-service">
                                <?php if (!empty($services_1['tieu_de'])) : ?>
                                    <h2 class="case-study-service__heading">
                                        <?= $services_1['tieu_de']; ?>
                                    </h2>
                                <?php endif; ?>
                                <?php if (!empty($services_1['ten_dich_vu'])) : ?>
                                    <h3 class="case-study-service__title">
                                        <?= $services_1['ten_dich_vu']; ?>
                                    </h3>
                                <?php endif; ?>
                                <?php if (!empty($services_1['mo_ta_ngan'])) : ?>
                                    <p class="case-study-service__button">
                                        <?= $services_1['mo_ta_ngan']; ?>
                                    </p>
                                <?php endif; ?>
                                <?php if (!empty($services_1['duong_dan'])) : ?>
                                    <a href="<?= $services_1['duong_dan']; ?>" class="btn">
                                        Xem thếm
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($services_2)) : ?>
                        <div class="ct-column-4">
                            <div class="case-study-service">
                                <?php if (!empty($services_2['tieu_de'])) : ?>
                                    <h2 class="case-study-service__heading">
                                        <?= $services_2['tieu_de']; ?>
                                    </h2>
                                <?php endif; ?>
                                <?php if (!empty($services_2['ten_dich_vu'])) : ?>
                                    <h3 class="case-study-service__title">
                                        <?= $services_2['ten_dich_vu']; ?>
                                    </h3>
                                <?php endif; ?>
                                <?php if (!empty($services_2['mo_ta_ngan'])) : ?>
                                    <p class="case-study-service__button">
                                        <?= $services_2['mo_ta_ngan']; ?>
                                    </p>
                                <?php endif; ?>
                                <?php if (!empty($services_2['duong_dan'])) : ?>
                                    <a href="<?= $services_2['duong_dan']; ?>" class="btn">
                                        Xem thếm
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($services_3)) : ?>
                        <div class="ct-column-4">
                            <div class="case-study-service">
                                <?php if (!empty($services_3['tieu_de'])) : ?>
                                    <h2 class="case-study-service__heading">
                                        <?= $services_3['tieu_de']; ?>
                                    </h2>
                                <?php endif; ?>
                                <?php if (!empty($services_3['ten_dich_vu'])) : ?>
                                    <h3 class="case-study-service__title">
                                        <?= $services_3['ten_dich_vu']; ?>
                                    </h3>
                                <?php endif; ?>
                                <?php if (!empty($services_3['mo_ta_ngan'])) : ?>
                                    <p class="case-study-service__button">
                                        <?= $services_3['mo_ta_ngan']; ?>
                                    </p>
                                <?php endif; ?>
                                <?php if (!empty($services_3['duong_dan'])) : ?>
                                    <a href="<?= $services_3['duong_dan']; ?>" class="btn">
                                        Xem thếm
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>

    </main><!-- #main -->

<?php
get_footer();
