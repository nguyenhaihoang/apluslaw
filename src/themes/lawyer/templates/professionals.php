<?php
/**
 * Template Name: Luật sư
 *
 * @package lawyer
 */

get_header();

$services_1 = get_field('service_1');
$services_2 = get_field('service_2');
$services_3 = get_field('service_3');
?>
    <header class="page-header">
        <div class="container-layout">
            <?php
            echo get_hansel_and_gretel_breadcrumbs();
            the_archive_title('<h1 class="page-title">', '</h1>');
            ?>
            <div class="page-description">
                <?php
                the_archive_description('<div class="archive-description">', '</div>');
                ?>
            </div>
        </div>
    </header><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <?php $selects = acf_get_fields('group_60c8d1e2458b1');?>
            <div class="page-filter">
                <form method="GET" action="<?php echo get_permalink(); ?>">
                    <div class="case-study__filter">
                        <div class="filter">
                            <div class="filter__search">
                                <div class="search-form">
                                    <label>
                                        <span class="screen-reader-text">Tìm kiếm cho:</span>
                                        <input type="search" class="search-field" placeholder="Tìm kiếm …" value="<?php echo !empty($_GET['search']) ? $_GET['search'] : ''; ?>" name="search">
                                    </label>
                                    <input type="submit" class="search-submit" value="Tìm kiếm">
                                </div>
                            </div>
                            <?php
                                if (!empty($selects)) :
                                $query_posts = array('relation' => 'AND');
                                foreach ($selects as $select) :
                                    if ($select['type'] != 'select') continue;
                                    if (empty($select['choices'])) continue;
                                ?>
                                <div class="filter__select">
                                    <select name="<?php echo $select['name'] ?>">
                                        <option value=""><?php echo $select['label']; ?></option>
                                        <?php
                                            foreach ($select['choices'] as $key => $value) :
                                                $selected = (!empty($_GET[$select['name']]) && $_GET[$select['name']] === $key) ? 'selected' : '';
                                                array_push($array_value,$key);
                                            ?>
                                                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
                                            <?php
                                            endforeach;
                                        ?>
                                    </select>
                                </div>
                            <?php
                                if (!empty($_GET[$select['name']])) :
                                    $array_data = array([
                                        'key'	 	=> $select['name'],
                                        'value'	  	=> $_GET[$select['name']],
                                        'compare' 	=> 'IN',
                                    ]);
                                    array_push($query_posts,$array_data);
                                endif;
                                endforeach;
                                endif;
                            ?>
                            <button class="filter-search">Tìm kiếm</button>
                        </div>
                        <div class="count">
                            <?php $count = $GLOBALS['wp_query']->post_count; echo $count;?> bài viết
                        </div>
                    </div>
                </form>
            </div>
            <?php
                $args = array(
                    'post_type' => 'professionals',
                    'showposts' => -1,
                );
                if (!empty($_GET['search'])) {
                    $args['s'] = $_GET['search'];
                }
                if (!empty($query_posts) && count($query_posts) > 1) {
                    $args['meta_query'] = $query_posts;
                }

                $my_query = new wp_query($args);
            ?>
            <?php
            if ($my_query->have_posts()) { ?>
                <div class="ct-row ct-row--col-five ct-row--doubling">
                <?php
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    echo '<div class="ct-column">';
                    get_template_part('template-parts/content', 'grid-professionals');
                    echo '</div>';
                }
                wp_reset_query();?>
                </div>
                <?php
            } ?>
        </div>
        <?php
        get_template_part('template-parts/components/services');
        ?>
    </main><!-- #main -->

<?php
get_footer();
