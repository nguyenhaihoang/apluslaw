<?php
/**
 * Template Name: Home page
 *
 * @package lawyer
 */

get_header();

// Thumbnail
$thumbnail = get_field('thumbnail');
// Section 1
$section_1 = get_field('section_1');
// Section 2
$section_2 = get_field('section_2');
// Section 3
$section_3 = get_field('section_3');
// Section 4
$section_4 = get_field('section_4');
// Section 4
$section_5 = get_field('section_5');
?>
    <div class="site-home">
        <section class="section-banner<?php if ( is_active_sidebar( 'topbar-widget' ) ) echo ' has-topbar'; ?>">
            <div class="site-banner">
                <?php if (!empty($thumbnail['image'])) : ?>
                    <img class="site-banner__bg" src="<?php echo $thumbnail['image']; ?>"
                         alt="<?php esc_html_e('Banner logo', 'lawyer'); ?>">
                <?php endif; ?>
                <?php if (!empty($thumbnail['video_id'])) : ?>
                    <video autoplay id="video" muted loop>
<!--                        <source src="https://lotus-apluslaw-1.s3-ap-southeast-1.amazonaws.com/lotusnew.mp4" type="video/mp4">-->
                        <source src="<?php echo $thumbnail['video_id']; ?>" type="video/mp4">
                    </video>
                <?php endif; ?>
            </div>
        </section>
        <section class="section-field">
            <div class="container-full">
                <?php if (!empty($section_1['section_heading'])) : ?>
                    <h2 class="heading">
                        <?php echo $section_1['section_heading']; ?>
                    </h2>
                    <?php if (!empty($section_1['section_sub_heading'])) : ?>
                        <div class="sub-heading">
                            <?php echo $section_1['section_sub_heading']; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if (!empty($section_1['section_heading'])) : ?>
                    <div class="s-field">
                        <div class="container-layout">
                            <div class="s-field__content">
                                <?php if (!empty($section_1['section_block_1_title'])) : ?>
                                    <h3 class="title"><?php echo $section_1['section_block_1_title']; ?></h3>
                                <?php endif; ?>
                                <?php if (!empty($section_1['section_block_1_content'])) : ?>
                                    <div class="description">
                                        <p><?php echo $section_1['section_block_1_content']; ?>
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php if (!empty($section_1['section_block_1_image'])) : ?>
                                <div class="s-field__image">
                                    <img src="<?php echo $section_1['section_block_1_image']; ?>"
                                         alt="<?php echo (!empty($section_1['section_block_1_title'])) ? $section_1['section_block_1_title'] : 'Home image' ?>">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (!empty($section_1['section_block_2_title'])) : ?>
                    <div class="s-field s-field--right-content">
                        <div class="container-layout">
                            <?php if (!empty($section_1['section_block_2_image'])) : ?>
                                <div class="s-field__image">
                                    <img src="<?php echo $section_1['section_block_2_image']; ?>"
                                         alt="<?php echo (!empty($section_1['section_block_2_title'])) ? $section_1['section_block_2_title'] : 'Home image' ?>">
                                </div>
                            <?php endif; ?>
                            <div class="s-field__content">
                                <?php if (!empty($section_1['section_block_2_title'])) : ?>
                                    <h3 class="title"><?php echo $section_1['section_block_2_title']; ?></h3>
                                <?php endif; ?>
                                <?php if (!empty($section_1['section_block_2_content'])) : ?>
                                    <div class="description">
                                        <p><?php echo $section_1['section_block_2_content']; ?>
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
        <section class="section-difference">
            <div class="container-layout">
                <?php if (!empty($section_2['section_heading'])) : ?>
                    <h2 class="heading">
                        Điểm khác biệt
                    </h2>
                <?php endif; ?>
                <div class="ct-row">
                    <?php if (!empty($section_2['section_block_1'])) : ?>
                        <div class="ct-column-4">
                            <div class="difference-item">
                                <?php if (!empty($section_2['section_block_1']['image'])) : ?>
                                    <div class="image">
                                        <img src="<?php echo $section_2['section_block_1']['image']; ?>"
                                             alt="<?php echo (!empty($section_1['section_block_1']['heading'])) ? $section_1['section_block_1']['heading'] : 'Home image' ?>">
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($section_2['section_block_1']['heading'])) : ?>
                                    <h2 class="title"><?php echo $section_2['section_block_1']['heading']; ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($section_2['section_block_1']['content'])) : ?>
                                    <p class="description"><?php echo $section_2['section_block_1']['content']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($section_2['section_block_2'])) : ?>
                        <div class="ct-column-4">
                            <div class="difference-item">
                                <?php if (!empty($section_2['section_block_2']['image'])) : ?>
                                    <div class="image">
                                        <img src="<?php echo $section_2['section_block_2']['image']; ?>"
                                             alt="<?php echo (!empty($section_2['section_block_2']['heading'])) ? $section_2['section_block_2']['heading'] : 'Home image' ?>">
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($section_2['section_block_2']['heading'])) : ?>
                                    <h2 class="title"><?php echo $section_2['section_block_2']['heading']; ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($section_2['section_block_2']['content'])) : ?>
                                    <p class="description"><?php echo $section_2['section_block_2']['content']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($section_2['section_block_3'])) : ?>
                        <div class="ct-column-4">
                            <div class="difference-item">
                                <?php if (!empty($section_2['section_block_3']['image'])) : ?>
                                    <div class="image">
                                        <img src="<?php echo $section_2['section_block_3']['image']; ?>"
                                             alt="<?php echo (!empty($section_2['section_block_3']['heading'])) ? $section_2['section_block_3']['heading'] : 'Home image' ?>">
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($section_2['section_block_3']['heading'])) : ?>
                                    <h2 class="title"><?php echo $section_2['section_block_3']['heading']; ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($section_2['section_block_3']['content'])) : ?>
                                    <p class="description"><?php echo $section_2['section_block_3']['content']; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if (!empty($section_2['site_link'])) : ?>
                    <div class="difference-link">
                        <a href="<?php echo $section_2['site_link']; ?>" class="section__link">
                            <?php echo esc_html_e("Read More About Us"); ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </section>
        <section class="section-lawyer">
            <div class="container-layout">
                <h2 class="heading">
                    Luật sư
                </h2>
                <div class="swiper-container lawyer-slider">
                    <div class="swiper-wrapper">
                        <?php if (!empty($section_3['post_id'])) :
                            $post_ids = $section_3['post_id'];
                            ?>
                            <?php
                            $args = array(
                                'post__in' => $post_ids,
                                'post_type' => 'professionals',
                            );
                            $my_query = new wp_query($args);
                            if ($my_query->have_posts()) {
                                while ($my_query->have_posts()) {
                                    $my_query->the_post();
                                    ?>
                                    <div class="swiper-slide">

                                        <div class="lawyer-item">
                                            <div class="image">
                                                <a href="#" class="<?php the_permalink(); ?>">
                                                    <?php the_post_thumbnail(); ?>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <?php if (!empty(get_field('info'))) : $info = get_field('info'); if(!empty($info['position'])) : ?>
                                                    <div class="section__sub-heading">
                                                        <span><?php echo $info['position']; ?></span>
                                                    </div>
                                                <?php endif;endif; ?>
                                                <h2 class="title">
                                                    <a href="<?php echo get_permalink(); ?>">
                                                        <?php echo get_the_title(); ?>
                                                    </a>
                                                </h2>
                                                <p class="description">
                                                    <?php
                                                    $excerpt = get_the_excerpt();
                                                    $excerpt = substr($excerpt, 0, 100);
                                                    $result = substr($excerpt, 0, strrpos($excerpt, ' '));
                                                    echo $result . '...';
                                                    ?>
                                                </p>
                                                <a href="<?php echo get_permalink(); ?>" class="site-link">
                                                    <?php esc_html_e('Read more'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                wp_reset_query();
                            }
                            ?>
                        <?php endif; ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </section>
        <section class="section case-study">
            <div class="container-layout">
                <div class="ct-row ct-row--doubling">
                    <?php if (!empty($section_4['section_heading'])) : ?>
                        <div class="ct-column-3">
                            <div class="case-study__heading">
                                <h2 class="heading">
                                    <?php echo $section_4['section_heading']; ?>
                                </h2>
                                <?php if (!empty($section_4['section_sub_heading'])) : ?>
                                    <p class="caption">
                                        <?php echo $section_4['section_sub_heading']; ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="ct-column-9">
                        <?php if (!empty($section_4['section_link'])) : ?>
                            <div class="case-study__viewmore">
                                <a href="#">
                                    Tất cả
                                </a>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($section_4['post_id'])) : ?>
                            <div class="case-study__list">
                                <?php
                                $post_ids = $section_4['post_id'];
                                ?>
                                <?php
                                $args = array(
                                    'post__in' => $post_ids,
                                    'post_type' => 'case_study',
                                );
                                $my_query = new wp_query($args);
                                if ($my_query->have_posts()) {
                                    while ($my_query->have_posts()) {
                                        $my_query->the_post();
                                        $id = get_the_ID();
                                        ?>
                                        <div class="ct-row">
                                            <div class="ct-column-6">
                                                <div class="case-study__item">
                                                    <h4 class="title">
                                                        <?php echo get_the_title(); ?>
                                                    </h4>
                                                    <div class="wrapper">
                                                        <p class="description">
                                                            <?php echo get_the_excerpt(); ?>
                                                        </p>
                                                        <a href="<?php the_permalink(); ?>" class="site-link">
                                                            Read more
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ct-column-3">
                                                <div class="date">
                                                    <span class="label">Năm:</span>
                                                    <span class="value"><?php
                                                        $terms = wp_get_object_terms($id, 'case_study_year', array('fields' => 'names'));
                                                        if (!empty($terms)) :
                                                            foreach ($terms as $term) :
                                                                ?>
                                                                <?php echo $term . ' '; ?>
                                                            <?php endforeach; endif; ?></span>
                                                </div>
                                                <div class="brand">
                                                    <span class="label">Thương Hiệu:</span><span class="value"> <?php $terms = wp_get_object_terms($id, 'case_study_brand', array('fields' => 'names'));
                                                        if (!empty($terms)) :
                                                            foreach ($terms as $term) :
                                                                ?>
                                                                <?php echo $term . ' '; ?>
                                                            <?php endforeach; endif; ?></span>
                                                </div>
                                            </div>
                                            <div class="ct-column-3">
                                                <div class="nation">
                                                    <span class="label">Quốc gia:</span> <span class="value"><?php
                                                        $terms = wp_get_object_terms($id, 'case_study_nation', array('fields' => 'names'));
                                                        if (!empty($terms)) :
                                                            foreach ($terms as $term) :
                                                                ?>
                                                                <?php echo $term; ?>
                                                            <?php endforeach; endif; ?>
                                                    </span>
                                                </div>
                                                <div class="province">
                                                    <span class="label">Tỉnh, thành phố:</span><span class="value"><?php
                                                        $terms = wp_get_object_terms($id, 'case_study_province', array('fields' => 'names'));
                                                        if (!empty($terms)) :
                                                            foreach ($terms as $term) :
                                                                ?>
                                                                <?php echo $term . ' '; ?>
                                                            <?php endforeach; endif; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                }; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-featured">
            <div class="container-layout">
                <?php if (!empty($section_5['section_heading'])) : ?>
                    <div class="featured__heading">
                        <h2><?php echo $section_5['section_heading']; ?></h2>
                        <?php if (!empty($section_5['section_link'])) : ?>
                            <a href="<?php echo $section_5['section_link']; ?>" class="link">
                                Tất cả
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($section_5['post_id'])) :
                    $post_ids = $section_5['post_id'];
                    ?>
                    <div class="ct-row ct-row--doubling">
                        <?php
                        $args = array(
                            'post__in' => $post_ids,
                        );
                        $my_query = new wp_query($args);
                        if ($my_query->have_posts()) {
                            while ($my_query->have_posts()) {
                                $my_query->the_post();
                                ?>
                                <div class="ct-column-4">
                                    <div class="grid-post">
                                        <div class="grid-post__image">
                                            <a href="#" class="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail(); ?>
                                            </a>
                                        </div>
                                        <span class="grid-post__date">
                                        <?php lawyer_posted_on(); ?>
                                    </span>
                                        <h2 class="grid-post__title">
                                            <a href="<?php echo get_permalink(); ?>">
                                                <?php echo get_the_title(); ?>
                                            </a>
                                        </h2>
                                        <p class="grid-post__description">
                                            <?php echo get_the_excerpt(); ?>
                                        </p>
                                        <a href="<?php echo get_permalink(); ?>" class="site-link">
                                            <?php esc_html_e('Read more'); ?>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                            wp_reset_query();
                        }
                        ?>
                    </div>
                <?php endif; ?>
        </section>
    </div>
<?php
get_footer();
