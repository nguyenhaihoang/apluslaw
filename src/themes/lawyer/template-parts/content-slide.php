<div class="home-slider__item">
    <span class="home-slider__sub-title">
        Thailand | Technology
    </span>
    <h3 class="home-slider__title">
        <?php echo get_the_title(); ?>
    </h3>
    <div class="home-slider__description">
        <p>
            <?php echo get_the_excerpt(); ?>
        </p>
    </div>
    <a href="<?php echo get_permalink(); ?>" class="section__link">
        Read More
    </a>
</div>