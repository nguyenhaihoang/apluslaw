<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lawyer
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <span class="section__sub-heading">
        <?php echo __('BIOGRAPHY', 'lawyer'); ?>
    </span>
    <div class="entry-content">
        <?php
        the_content(
            sprintf(
                wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                    __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'lawyer'),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                wp_kses_post(get_the_title())
            )
        );

        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . esc_html__('Pages:', 'lawyer'),
                'after' => '</div>',
            )
        );
        ?>
    </div><!-- .entry-content -->
    <?php if (get_field('experience')): ?>
        <div class="single-experience">
        <span class="section__sub-heading">
            <?php echo __('EXPERIENCE', 'lawyer'); ?>
        </span>
            <div class="entry-content">
                <?php the_field('experience'); ?>
            </div>
        </div>
    <?php endif; ?>
    <footer class="entry-footer">
        <?php lawyer_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
