<div class="grid-post">
    <div class="grid-post__thumbnail-image">
        <a href="<?php echo get_permalink(); ?>">
            <?php echo get_the_post_thumbnail(); ?>
        </a>
    </div>
    <span class="grid-post__date">
        <?php lawyer_posted_on(); ?>
    </span>
    <h2 class="grid-post__title">
        <a href="<?php echo get_permalink(); ?>">
            <?php echo get_the_title(); ?>
        </a>
    </h2>
    <p class="grid-post__description">
        <?php echo get_the_excerpt(); ?>
    </p>
    <a href="<?php echo get_permalink(); ?>" class="site-link">
        <?php esc_html_e('Read more'); ?>
    </a>
</div>