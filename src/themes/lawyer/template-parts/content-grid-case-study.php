<div class="grid-post professionals">
    <div class="grid-post__thumbnail">
        <a href="<?php echo get_permalink(); ?>">
            <?php lawyer_post_thumbnail(); ?>
        </a>
    </div>
    <?php
        $id=get_the_ID();
        $terms =  wp_get_object_terms( $id, 'case_study_category', array('fields'=>'names'));
        if (!empty( $terms[0] )) :
    ?>
        <div class="grid-post__category">
            <span>
                <?php echo $terms[0]; ?>
            </span>
        </div>
    <?php endif; ?>
    <h2 class="grid-post__title">
        <a href="<?php echo get_permalink(); ?>">
            <?php echo get_the_title(); ?>
        </a>
    </h2>
    <?php if( get_field('info') ):
        $info = get_field('info')
        ?>
        <p class="grid-post__position">
            <?php echo $info['position']; ?>
        </p>
    <?php endif; ?>
    <p class="grid-post__description">
        <?php
        $excerpt = get_the_excerpt();

        $excerpt = substr($excerpt, 0, 100);
        $result = substr($excerpt, 0, strrpos($excerpt, ' '));
        echo $result . '...';
        ?>
    </p>
</div>