
<?php $pageID = get_option('page_on_front'); ?>
<?php
$args = array(
    'post_type' => 'page',
    'post__in' => array($pageID),
);
$my_query = new wp_query($args);
if ($my_query->have_posts()) {
    while ($my_query->have_posts()) {
        $my_query->the_post();
        $section_1 = get_field('section_1');
        ?>
        <section class="services-field">
            <div class="container-layout">
                <div class="ct-row">
                    <div class="ct-column-6">
                        <div class="services-field__item">
                                    <span class="services-field__background">
                                        <?php if (!empty($section_1['section_block_1_title'])) : ?>
                                            <img src="<?php echo $section_1['section_block_1_image']; ?>" alt="<?php echo !empty($section_1['section_block_1_title']) ? $section_1['section_block_1_title'] : 'Section image'; ?>">
                                        <?php endif; ?>
                                    </span>
                            <div class="services-field__content">
                                <?php if (!empty($section_1['section_block_1_title'])) : ?>
                                    <h2 class="services-field__title">
                                        <?php echo $section_1['section_block_1_title']; ?>
                                    </h2>
                                <?php endif; ?>
                                <?php if (!empty($section_1['section_block_1_content'])) : ?>
                                    <p class="services-field__description">
                                        <?php echo $section_1['section_block_1_content']; ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="ct-column-6">
                        <div class="services-field__item">
                                    <span class="services-field__background">
                                        <?php if (!empty($section_1['section_block_2_image'])) : ?>
                                            <img src="<?php echo $section_1['section_block_2_image']; ?>"  alt="<?php echo !empty($section_1['section_block_2_title']) ? $section_1['section_block_2_title'] : 'Section image'; ?>">
                                        <?php endif; ?>
                                    </span>
                            <div class="services-field__content">
                                <?php if (!empty($section_1['section_block_2_title'])) : ?>
                                    <h2 class="services-field__title">
                                        <?php echo $section_1['section_block_2_title']; ?>
                                    </h2>
                                <?php endif; ?>
                                <?php if (!empty($section_1['section_block_2_content'])) : ?>
                                    <p class="services-field__description">
                                        <?php echo $section_1['section_block_2_content']; ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
    }
    wp_reset_query();
} ?>