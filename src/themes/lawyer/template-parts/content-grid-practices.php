<div class="grid-post professionals">
    <div class="grid-post__thumbnail">
        <a href="<?php echo get_permalink(); ?>">
            <?php lawyer_post_thumbnail(); ?>
        </a>
    </div>
    <h2 class="grid-post__title">
        <a href="<?php echo get_permalink(); ?>">
            <?php echo get_the_title(); ?>
        </a>
    </h2>
    <a href="<?php echo get_permalink(); ?>" class="site-link">
        <?php echo __('Learn more','lawyer'); ?>
    </a>
</div>