<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lawyer
 */

get_header();
?>
    <header class="page-header">
        <div class="container-layout">
            <?php
            echo get_hansel_and_gretel_breadcrumbs();
            the_archive_title('<h1 class="page-title">', '</h1>');
            ?>
            <div class="page-description">
                <?php the_archive_description('<div class="archive-description">', '</div>'); ?>
            </div>
        </div>
    </header><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <?php if (have_posts()) : ?>
                <div class="ct-row ct-row--doubling">
                    <?php
                    /* Start the Loop */
                    while (have_posts()) :
                        the_post();
                        echo '<div class="ct-column-3">';
                        get_template_part('template-parts/content', 'grid-practices');
                        echo '</div>';
                    endwhile;

                    ?>
                </div>
            <?php
                the_posts_navigation();
            else :

                get_template_part('template-parts/content', 'none');

            endif;
            ?>
        </div>
        <section class="archive-categorys">
            <div class="container-layout">
                <div class="ct-row">
                <?php
                $taxonomies = get_taxonomies(['object_type' => ['practices']]);
                $taxonomyTerms = [];
                // loop over your taxonomies
                foreach ($taxonomies as $taxonomy)
                {
                    // retrieve all available terms, including those not yet used
                    $terms    = get_terms(['taxonomy' => $taxonomy, 'hide_empty' => false]);

                    // make sure $terms is an array, as it can be an int (count) or a WP_Error
                    $hasTerms = is_array($terms) && $terms;

                    if($hasTerms)
                    {
                        $taxonomyTerms[$taxonomy] = $terms;
                    }
                }
                foreach ($terms as $term) :
                ?>
                <div class="ct-column-6">
                    <a href="<?php echo get_term_link($term->term_id) ?>" class="archive-categorys__item">
                        <?php echo $term->name; ?>
                    </a>
                </div>
                <?php endforeach; ?>
                </div>
            </div>
        </section>

    </main><!-- #main -->

<?php
get_footer();
