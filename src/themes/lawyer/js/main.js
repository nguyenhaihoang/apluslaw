import SwiperCore, {Pagination} from 'swiper/core';
import 'owl.carousel';

SwiperCore.use([Pagination])


jQuery(document).ready(function ($) {

    const headerScroll = function () {
        const header = $('.site-header');
        let scroll = $(window).scrollTop();

        if (scroll > 10) {
            header.addClass('is-scroll');
        }

        $(window).on('scroll', function (e) {
            scroll = $(this).scrollTop();
            if (scroll > 10) {
                header.addClass('is-scroll');
            } else {
                header.removeClass('is-scroll');
            }
        })
    };

    const homeSlider = function () {
        if ($('.home-slider').length > 0) {
            const swiper = new SwiperCore('.home-slider', {
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                },
            });
        }
    }

    const lawyerSlider = function () {
        if ($('.lawyer-slider').length > 0){
            const swiper = new SwiperCore('.lawyer-slider', {
                loop: true,
                slidesPerView: 1,
                spaceBetween: 15,
                autoplay: {
                    delay: 3000,
                },
                pagination: {
                    el: '.swiper-pagination',
                },
                breakpoints: {
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 15,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 30,
                    },
                },
            });
        }
    }

    const searchOpen = function () {
        const search_popup = $('.search-popup');

        $(document).on('click', '.search-open', function (e) {
            e.preventDefault();
            search_popup.addClass('is-open');
        })

        $(document).on('click', function (e) {
            if ($(e.target).hasClass('search-popup')) {
                search_popup.removeClass('is-open');
            }
        })
    }

    const onScroll = function () {
        $(document).on('click', '.on-scroll', function (e) {
            const id = $(this).attr('href');
            $('html,body').animate({scrollTop: $(id).offset().top}, 'slow');
        })
    }

    const menuToggle = function () {
        let controls;
        $(document).on('click', '.menu-toggle', function (e) {
            e.preventDefault();
            const $_this = $(this);
            controls = $_this.attr('aria-controls');
            $('#' + controls).toggleClass('is-active');
            $('body').toggleClass('is-scroll');
        })
        $(document).on('click', function (e) {
            if ($(e.target).hasClass('is-scroll')) {
                $('#' + controls).removeClass('is-active');
                $('body').removeClass('is-scroll');
            }
        })
    }

    function caseStudyList() {
        $(document).on('click','.case-study__list .ct-row',function (e){
            e.preventDefault();
            let _this = $(this);
            _this.siblings().removeClass('is-active');
            _this.siblings().find('.case-study__item .wrapper').slideUp();
            _this.addClass('is-active');
            _this.find('.case-study__item .wrapper').slideDown();
        })
    }
    function  caseStudySlide() {
        const case_study = $('.case-study__slide');
        if ($('.lawyer-slider').length <= 0) return;
        // case_study.find('.ct-row').owlCarousel();
    }
    // init
    lawyerSlider();
    // headerScroll();
    homeSlider();
    searchOpen();
    onScroll();
    menuToggle();
    caseStudyList();
    caseStudySlide();

})
