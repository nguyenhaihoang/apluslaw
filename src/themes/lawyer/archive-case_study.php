<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lawyer
 */

get_header();
?>
    <div class="page-header">
        <div class="container-layout">
            <?php
            echo get_hansel_and_gretel_breadcrumbs();
            the_archive_title('<h1 class="page-title">', '</h1>');
            ?>
        </div>
    </div><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <div class="ct-row">
                <div class="ct-column-3">
                    <?php
                    $taxonomies = get_taxonomies(['object_type' => ['case_study']]);
                    $taxonomyTerms = [];
                    // loop over your taxonomies
                    if (!empty($taxonomies)) :
                        foreach ($taxonomies as $taxonomy) {
                            // retrieve all available terms, including those not yet used
                            $terms = get_terms(['taxonomy' => $taxonomy, 'hide_empty' => false]);
                            // make sure $terms is an array, as it can be an int (count) or a WP_Error
                            $hasTerms = is_array($terms) && $terms;

                            if ($hasTerms) {
                                $taxonomyTerms[$taxonomy] = $terms;
                            }
                        } ?>
                        <?php
                        foreach ($taxonomyTerms as $key => $terms) : if ($key === 'case_study_brand' || $key === 'case_study_province') continue;?>
                            <div class="case-study__sidebar">
                                <h2><?php
                                    switch ($key) {
                                        case 'case_study_category':
                                            echo 'Lĩnh vực';
                                            break;
                                        case 'case_study_year':
                                            echo 'Năm';
                                            break;
                                        case 'case_study_brand':
                                            echo 'Thương hiệu';
                                            break;
                                        case 'case_study_nation':
                                            echo 'Quốc gia';
                                            break;
                                        case 'case_study_province':
                                            echo 'Tỉnh';
                                            break;
                                        default:
                                            echo 'Danh mục';
                                    }
                                ?>
                                </h2>
                                <ul>
                                    <?php foreach ($terms as $term) :
                                        ?>
                                        <li>
                                            <a href="<?php echo get_term_link($term->term_id) ?>">
                                                <?php echo $term->name; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="ct-column-9">
                    <div class="case-study__filter">
                        <div class="filter">
                            <div class="filter__search">
                                <form method="get" class="search-form" action="./">
                                    <label>
                                        <span class="screen-reader-text">Tìm kiếm cho:</span>
                                        <input type="search" class="search-field" placeholder="Tìm kiếm …" value="" name="s">
                                    </label>
                                    <input type="submit" class="search-submit" value="Tìm kiếm">
                                </form>
                            </div>
                        </div>
                        <div class="count">
                            <a href="#"><?php $count = $GLOBALS['wp_query']->post_count; echo $count;?> bài viết</a>
                        </div>
                    </div>
                    <div class="case-study__posts">
                        <?php if (have_posts()) : ?>
                            <div class="ct-row ct-row--doubling">
                                <?php
                                /* Start the Loop */
                                while (have_posts()) :
                                    the_post();
                                    echo '<div class="ct-column ct-column-4">';
                                    get_template_part('template-parts/content', 'grid-case-study');
                                    echo '</div>';
                                endwhile;
                                ?>
                            </div>
                            <?php
                            the_posts_navigation();
                        else :
                            get_template_part('template-parts/content', 'none');
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <section class="case-study__services">
            <div class="container-layout">
                <h2 class="s-heading">
                    A+ có thể giúp được gì cho bạn?
                </h2>
                <div class="ct-row">
                    <div class="ct-column-4">
                        <div class="case-study-service">
                            <h2 class="case-study-service__heading">
                                Dịch vụ 1
                            </h2>
                            <h3 class="case-study-service__title">
                                Tên dịch vụ 1
                            </h3>
                            <p class="case-study-service__button">
                                Mô tả ngắn
                            </p>
                            <a href="#" class="btn">
                                Xem thếm
                            </a>
                        </div>
                    </div>
                    <div class="ct-column-4">
                        <div class="case-study-service">
                            <h2 class="case-study-service__heading">
                                Dịch vụ 1
                            </h2>
                            <h3 class="case-study-service__title">
                                Tên dịch vụ 1
                            </h3>
                            <p class="case-study-service__button">
                                Mô tả ngắn
                            </p>
                            <a href="#" class="btn">
                                Xem thếm
                            </a>
                        </div>
                    </div>
                    <div class="ct-column-4">
                        <div class="case-study-service">
                            <h2 class="case-study-service__heading">
                                Dịch vụ 1
                            </h2>
                            <h3 class="case-study-service__title">
                                Tên dịch vụ 1
                            </h3>
                            <p class="case-study-service__button">
                                Mô tả ngắn
                            </p>
                            <a href="#" class="btn">
                                Xem thếm
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main><!-- #main -->

<?php
get_footer();
