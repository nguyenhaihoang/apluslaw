<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lawyer
 */

get_header();
?>

    <div class="page-header page-header--thumbnail">
        <?php // lawyer_post_thumbnail(); ?>
        <div class="container-layout">
            <?php echo get_hansel_and_gretel_breadcrumbs(); ?>
            <div  style="max-width: 900px;text-align: center">
                <?php the_title('<h2 class="page-single-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>'); ?>
                <span class="page-description"><?php echo get_the_excerpt(); ?></span>
            </div>
        </div>
    </div><!-- .page-header -->
    <main id="primary" class="site-page single-content--style">
        <div class="container-layout">
            <div class="single-row">
                <div class="single-container-full">
                    <a href="<?php echo get_site_url(); ?>" class="site-back">
                        <?php echo __('Back to Blogs'); ?>
                    </a>
                    <div class="single-content">
                        <?php
                        while (have_posts()) :
                            the_post();

                            get_template_part('template-parts/content', get_post_type());

                        endwhile; // End of the loop.
                        ?>
                    </div>
                </div>
            </div>
            <section class="section single-related">
                <?php
                $categories = get_the_category(get_the_ID());
                if ($categories) {
                    $category_ids = array();
                    foreach ($categories as $individual_category) $category_ids[] = $individual_category->term_id;

                    $args = array(
                        'category__in' => $category_ids,
                        'post__not_in' => array(get_the_ID()),
                        'showposts' => 4,
                        'caller_get_posts' => 1
                    );
                    $my_query = new wp_query($args);
                    if ($my_query->have_posts()) {
                        echo '<h2 class="s-heading">Tin tức liên quan</h2>';
                        echo '<div class="ct-row ct-row--doubling">';
                        while ($my_query->have_posts()) {
                            $my_query->the_post();
                            echo '<div class="ct-column-3">';
                             get_template_part('template-parts/content', 'grid');
                            echo '</div>';
                        }
                        echo '</div>';
                    }
                    ?>

                    <div class="single-related__read-more">
                        <a href="<?php echo get_category_link($category_ids); ?>" class="btn">
                            Load more
                        </a>
                    </div>
                    <?php
                }
                ?>
            </section>

        </div>
    </main><!-- #main -->

<?php
get_footer();
