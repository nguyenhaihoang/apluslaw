<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lawyer
 */

get_header('white');
?>

    <main id="primary" class="site-main">
        <div class="container-layout">
            <?php
            while (have_posts()) :
                the_post();
                $id_post = get_the_ID();
                ?>
                <header class="single-header">
                    <div class="single-header__thumbnail">
                        <?php lawyer_post_thumbnail(); ?>
                    </div>
                    <?php $info = get_field('info'); ?>
                    <div class="single-header__info">
                        <div class="single-header__title">
                            <?php
                            if (is_singular()) :
                                the_title('<h1 class="entry-title">', '</h1>');
                            else :
                                the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
                            endif;
                            ?>
                            <?php if (!empty($info) && !empty($info['position'])) : ?>
                                <div class="single-header__position">
                                    <span>
                                        <?php echo $info['position']; ?>
                                    </span>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php if (!empty($info['phone'])) : ?>
                            <div class="single-header__field">
                                <a href="tel: <?php echo $info['phone']; ?>">
                                    <span><svg xmlns="http://www.w3.org/2000/svg" width="13" height="13"
                                               viewBox="0 0 13 13" fill="none"><path fill-rule="evenodd"
                                                                                     clip-rule="evenodd"
                                                                                     d="M2.61444 5.62611C3.65444 7.67 5.33 9.33833 7.37389 10.3856L8.96278 8.79667C9.15778 8.60167 9.44667 8.53667 9.69944 8.62333C10.5083 8.89056 11.3822 9.035 12.2778 9.035C12.675 9.035 13 9.36 13 9.75722V12.2778C13 12.675 12.675 13 12.2778 13C5.49611 13 0 7.50389 0 0.722222C0 0.325 0.325 0 0.722222 0H3.25C3.64722 0 3.97222 0.325 3.97222 0.722222C3.97222 1.625 4.11667 2.49167 4.38389 3.30056C4.46333 3.55333 4.40556 3.835 4.20333 4.03722L2.61444 5.62611Z"
                                                                                     fill="#00172B"></path></svg></span>
                                    <?php echo $info['phone']; ?></a>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($info['email'])) : ?>
                            <div class="single-header__field">
                                <a href="mailto:<?php echo $info['email']; ?>">
                                    <span><svg xmlns="http://www.w3.org/2000/svg" width="13" height="11"
                                               viewBox="0 0 13 11" fill="none"><path fill-rule="evenodd"
                                                                                     clip-rule="evenodd"
                                                                                     d="M12.1875 0H0.8125C0.364 0 0 0.351214 0 0.785714V10.2143C0 10.6488 0.364 11 0.8125 11H12.1875C12.636 11 13 10.6488 13 10.2143V0.785714C13 0.351214 12.636 0 12.1875 0ZM11.375 4.49664V9.42857H1.625V4.49664L6.097 6.96771C6.34725 7.106 6.65356 7.106 6.903 6.96771L11.375 4.49664ZM6.5 5.38136L11.375 2.68714V1.57143H1.625V2.68714L6.5 5.38136Z"
                                                                                     fill="#00172B"></path></svg></span>
                                    <?php echo $info['email']; ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </header>
                <div class="single-row">
                    <div class="single-container">
                        <div class="single-content single-content--no-boder">
                            <?php
                            get_template_part('template-parts/content', get_post_type());
                            ?>
                        </div>
                    </div>
                    <div class="single-sidebar">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            <?php
            endwhile; // End of the loop.
            ?>
            <div class="single-related">
                <?php
                $post_ids = get_field('insights');
                $args = array(
                    'post_type' => 'post',
                    'post__in' => $post_ids,
                    'showposts' => -1,
                );
                $my_query = new wp_query($args);
                if ($my_query->have_posts()) {
                    echo '<h3 class="section__sub-heading">INSIGHTS POST</h3>';
                    echo '<div class="ct-row ct-row--doubling">';
                    while ($my_query->have_posts()) {
                        $my_query->the_post();
                        echo '<div class="ct-column-3">';
                        get_template_part('template-parts/content', 'grid');
                        echo '</div>';
                    }
                    echo '</div>';
                    wp_reset_query();
                } ?>
            </div>
            <div class="single-related">
                <?php
                $post_ids = get_field('awards_rankings_post');
                $args = array(
                    'post_type' => 'post',
                    'post__in' => $post_ids,
                    'showposts' => -1,
                );
                $my_query = new wp_query($args);
                if ($my_query->have_posts()) {
                    echo '<h3 class="section__sub-heading">AWARDS & RANKINGS POST</h3>';
                    echo '<div class="ct-row ct-row--doubling">';
                    while ($my_query->have_posts()) {
                        $my_query->the_post();
                        echo '<div class="ct-column-3">';
                        get_template_part('template-parts/content', 'grid');
                        echo '</div>';
                    }
                    echo '</div>';
                    wp_reset_query();
                } ?>
            </div>
            <div class="single-related">
                <?php
                $args = array(
                    'post_type' => 'professionals',
                    'post__not_in' => array($id_post),
                    'showposts' => -1,
                );
                $my_query = new wp_query($args);
                if ($my_query->have_posts()) {
                    echo '<h3 class="section__sub-heading">OTHER PROFESSIONALS</h3>';
                    echo '<div class="ct-row ct-row--col-five ct-row--doubling">';
                    while ($my_query->have_posts()) {
                        $my_query->the_post();
                        echo '<div class="ct-column">';
                        get_template_part('template-parts/content', 'grid-professionals');
                        echo '</div>';
                    }
                    echo '</div>';
                    wp_reset_query();
                } ?>
            </div>
        </div>
    </main><!-- #main -->

<?php
get_footer();
