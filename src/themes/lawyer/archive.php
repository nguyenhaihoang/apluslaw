<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lawyer
 */

get_header();
?>
    <header class="page-header">
        <div class="container-layout">
            <?php
            echo get_hansel_and_gretel_breadcrumbs();
            the_archive_title('<h1 class="page-title">', '</h1>');
            ?>
            <div class="page-description">
                <?php
                the_archive_description('<div class="archive-description">', '</div>');
                ?>
            </div>
        </div>
    </header><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <div class="ct-row">
                <div class="ct-column-3">
                    <?php
                    $taxonomies = get_taxonomies(['object_type' => ['post']]);
                    $taxonomyTerms = [];
                    // loop over your taxonomies
                    if (!empty($taxonomies)) :
                        foreach ($taxonomies as $taxonomy) {
                            // retrieve all available terms, including those not yet used
                            $terms = get_terms(['taxonomy' => $taxonomy, 'hide_empty' => false]);
                            // make sure $terms is an array, as it can be an int (count) or a WP_Error
                            $hasTerms = is_array($terms) && $terms;

                            if ($hasTerms) {
                                $taxonomyTerms[$taxonomy] = $terms;
                            }
                        } ?>
                        <?php
                        foreach ($taxonomyTerms as $key => $terms) : if ($key === 'case_study_brand' || $key === 'case_study_province') continue;?>
                            <div class="case-study__sidebar">
                                <h2>
                                    Danh mục
                                </h2>
                                <ul>
                                    <?php foreach ($terms as $term) :
                                        ?>
                                        <li>
                                            <a href="<?php echo get_term_link($term->term_id) ?>">
                                                <?php echo $term->name; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="ct-column-9">
                    <div class="case-study__filter">
                        <div class="filter">
                            <div class="filter__search">
                                <?php echo get_search_form(); ?>
                            </div>
                        </div>
                        <div class="count">
                            <?php $count = $GLOBALS['wp_query']->post_count; echo $count;?> bài viết
                        </div>
                    </div>
                    <div class="case-study__posts">
                        <?php if (have_posts()) : ?>
                            <div class="ct-row ct-row--doubling">
                                <?php
                                /* Start the Loop */
                                while (have_posts()) :
                                    the_post();
                                    echo '<div class="ct-column-4">';
                                        get_template_part('template-parts/content', 'grid');
                                    echo '</div>';
                                endwhile;

                                ?>
                            </div>
                            <?php
                            the_posts_navigation();
                        else :

                            get_template_part('template-parts/content', 'none');

                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part('template-parts/components/services'); ?>
    </main><!-- #main -->

<?php
get_footer();
