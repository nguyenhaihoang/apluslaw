<?php
/**
 * The template for displaying blog pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lawyer
 */

get_header();
?>
    <div class="page-header">
        <div class="container-layout">
            <h1 class="page-title"><?php echo single_post_title(); ?></h1>
        </div>
    </div><!-- .page-header -->
    <div class="site-page__filter">
        <div class="container-layout">
            <h2 class="section__title">Tìm kiếm trên Apluslaw</h2>
            <div class="site-page__search">
                <?php echo get_search_form(); ?>
            </div>
        </div>
    </div>
    <main id="primary" class="site-page">
        <div class="container-layout">
            <?php if (have_posts()) : ?>
                <div class="ct-row ct-row--doubling">
                    <?php
                    /* Start the Loop */
                    while (have_posts()) :
                        the_post();
                        echo '<div class="ct-column-3">';
                        get_template_part('template-parts/content', 'grid');
                        echo '</div>';
                    endwhile;

                    ?>
                </div>
                <?php
                the_posts_navigation();
            else :

                get_template_part('template-parts/content', 'none');

            endif;
            ?>
        </div>

    </main><!-- #main -->

<?php
get_footer();
