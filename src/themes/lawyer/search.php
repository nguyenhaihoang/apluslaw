<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package lawyer
 */

get_header();
?>

    <div class="page-header">
        <div class="container-layout">
            <?php echo get_hansel_and_gretel_breadcrumbs(); ?>
            <h1 class="page-title">
                <?php
                printf(esc_html__('Tìm kiếm: %s', 'lawyer'), '<span>' . get_search_query() . '</span>');
                ?>
            </h1>
            <div class="page-description">
                <?php echo get_the_excerpt(); ?>
            </div>
        </div>
    </div><!-- .page-header -->
    <div class="site-page__filter">
        <div class="container-layout">
            <h2 class="section__title">Tìm kiếm</h2>
            <div class="site-page__search">
                <?php echo get_search_form(); ?>
            </div>
        </div>
    </div>
    <main id="primary" class="site-page">
        <div class="container-layout">
        <?php if (have_posts()) : ?>
            <div class="ct-row ct-row--doubling">
                <?php
                /* Start the Loop */
                while (have_posts()) :
                    the_post();
                    $post_type = get_post_type() != 'post' && get_post_type() != 'page' ? 'grid-' . get_post_type() : 'grid';
                    ?>
                <div class="ct-column-4">
                    <?php get_template_part('template-parts/content', $post_type); ?>
                </div>
                <?php
                endwhile;
                ?>
            </div>
            <?php
            the_posts_navigation();

        else :

            get_template_part('template-parts/content', 'none');

        endif;
        ?>
        </div>
    </main><!-- #main -->

<?php
get_footer();
