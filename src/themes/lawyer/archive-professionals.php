<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lawyer
 */

get_header();
?>
    <div class="page-header">
        <div class="container-layout">
            <?php
            echo get_hansel_and_gretel_breadcrumbs();
            the_archive_title('<h1 class="page-title">', '</h1>');
            ?>
            <div class="page-description">
                <?php the_archive_description('<div class="archive-description">', '</div>'); ?>
            </div>
        </div>
    </div><!-- .page-header -->
    <main id="primary" class="site-page">
        <div class="container-layout">
            <?php if (have_posts()) : ?>
                <div class="ct-row ct-row--col-five ct-row--doubling">
                    <?php
                    /* Start the Loop */
                    while (have_posts()) :
                        the_post();
                        echo '<div class="ct-column">';
                        get_template_part('template-parts/content', 'grid-professionals');
                        echo '</div>';
                    endwhile;

                    ?>
                </div>
                <?php
                the_posts_navigation();
            else :

                get_template_part('template-parts/content', 'none');

            endif;
            ?>
        </div>
        <?php
            get_template_part('template-parts/components/services');
        ?>
    </main><!-- #main -->

<?php
get_footer();
