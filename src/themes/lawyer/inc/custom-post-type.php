<?php
// Register Custom Post Type professionals
function create_professionals_cpt()
{
    $_prefix = 'Luật sư';
    $labels = array(
        'name' => _x($_prefix, 'Post Type General Name', 'lawyer'),
        'singular_name' => _x($_prefix, 'Post Type Singular Name', 'lawyer'),
        'menu_name' => _x($_prefix, 'Admin Menu text', 'lawyer'),
        'name_admin_bar' => _x($_prefix, 'Add New on Toolbar', 'lawyer'),
        'archives' => __($_prefix, 'lawyer'),
        'attributes' => __($_prefix, 'lawyer'),
        'parent_item_colon' => __($_prefix . ' gốc:', 'lawyer'),
        'all_items' => __('Tất cả ' . $_prefix, 'lawyer'),
        'add_new_item' => __('Thêm mới ' . $_prefix, 'lawyer'),
        'add_new' => __('Thêm mới', 'lawyer'),
        'new_item' => __($_prefix . ' mới', 'lawyer'),
        'edit_item' => __('Chỉnh sửa ' . $_prefix, 'lawyer'),
        'update_item' => __('Cập nhật' . $_prefix, 'lawyer'),
        'view_item' => __('Xem ' . $_prefix, 'lawyer'),
        'view_items' => __('Xem ' . $_prefix, 'lawyer'),
        'search_items' => __('Tìm kiếm' . $_prefix, 'lawyer'),
        'not_found' => __('Not found', 'lawyer'),
        'not_found_in_trash' => __('Not found in Trash', 'lawyer'),
        'featured_image' => __('Featured Image', 'lawyer'),
        'set_featured_image' => __('Set featured image', 'lawyer'),
        'remove_featured_image' => __('Remove featured image', 'lawyer'),
        'use_featured_image' => __('Use as featured image', 'lawyer'),
    );
    $args = array(
        'label' => __('professionals', 'lawyer'),
        'description' => __('', 'lawyer'),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-users',
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'luat-su')
    );
    register_post_type('professionals', $args);

}

add_action('init', 'create_professionals_cpt', 0);

// Register Custom Post Type practices
function create_practices_cpt() {

    $_prefix = 'Dịch vụ';
    $labels = array(
        'name' => _x($_prefix, 'Post Type General Name', 'lawyer'),
        'singular_name' => _x($_prefix, 'Post Type Singular Name', 'lawyer'),
        'menu_name' => _x($_prefix, 'Admin Menu text', 'lawyer'),
        'name_admin_bar' => _x($_prefix, 'Add New on Toolbar', 'lawyer'),
        'archives' => __($_prefix, 'lawyer'),
        'attributes' => __($_prefix, 'lawyer'),
        'parent_item_colon' => __($_prefix . ' gốc:', 'lawyer'),
        'all_items' => __('Tất cả ' . $_prefix, 'lawyer'),
        'add_new_item' => __('Thêm mới ' . $_prefix, 'lawyer'),
        'add_new' => __('Thêm mới', 'lawyer'),
        'new_item' => __($_prefix . ' mới', 'lawyer'),
        'edit_item' => __('Chỉnh sửa ' . $_prefix, 'lawyer'),
        'update_item' => __('Cập nhật' . $_prefix, 'lawyer'),
        'view_item' => __('Xem ' . $_prefix, 'lawyer'),
        'view_items' => __('Xem ' . $_prefix, 'lawyer'),
        'search_items' => __('Tìm kiếm' . $_prefix, 'lawyer'),
        'not_found' => __('Not found', 'lawyer'),
        'not_found_in_trash' => __('Not found in Trash', 'lawyer'),
        'featured_image' => __('Featured Image', 'lawyer'),
        'set_featured_image' => __('Set featured image', 'lawyer'),
        'remove_featured_image' => __('Remove featured image', 'lawyer'),
        'use_featured_image' => __('Use as featured image', 'lawyer'),
    );
    $args = array(
        'label' => __( 'practices', 'lawyer' ),
        'description' => __( '', 'lawyer' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-building',
        'supports' => array('title', 'thumbnail', 'revisions','editor', 'excerpt'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => array('slug' => 'dich-vu-aplus-law')
    );
    register_post_type( 'practices', $args );

}
add_action( 'init', 'create_practices_cpt', 0 );
// Register Custom Post Type practices

function create_case_study_cpt() {

    $labels = array(
        'name' => _x( 'Case study', 'Post Type General Name', 'lawyer' ),
        'singular_name' => _x( 'Case study', 'Post Type Singular Name', 'lawyer' ),
        'menu_name' => _x( 'Case study', 'Admin Menu text', 'lawyer' ),
        'name_admin_bar' => _x( 'Case study', 'Add New on Toolbar', 'lawyer' ),
        'archives' => __( 'Case study Archives', 'lawyer' ),
        'attributes' => __( 'Case study Attributes', 'lawyer' ),
        'parent_item_colon' => __( 'Parent Case study:', 'lawyer' ),
        'all_items' => __( 'All Case study', 'lawyer' ),
        'add_new_item' => __( 'Add New Case study', 'lawyer' ),
        'add_new' => __( 'Add New', 'lawyer' ),
        'new_item' => __( 'New Case study', 'lawyer' ),
        'edit_item' => __( 'Edit Case study', 'lawyer' ),
        'update_item' => __( 'Update Case study', 'lawyer' ),
        'view_item' => __( 'View Case study', 'lawyer' ),
        'view_items' => __( 'View Case study', 'lawyer' ),
        'search_items' => __( 'Search Case study', 'lawyer' ),
        'not_found' => __( 'Not found', 'lawyer' ),
        'not_found_in_trash' => __( 'Not found in Trash', 'lawyer' ),
        'featured_image' => __( 'Featured Image', 'lawyer' ),
        'set_featured_image' => __( 'Set featured image', 'lawyer' ),
        'remove_featured_image' => __( 'Remove featured image', 'lawyer' ),
        'use_featured_image' => __( 'Use as featured image', 'lawyer' ),
        'insert_into_item' => __( 'Insert into practices', 'lawyer' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Case study', 'lawyer' ),
        'items_list' => __( 'Case study list', 'lawyer' ),
        'items_list_navigation' => __( 'Case study list navigation', 'lawyer' ),
        'filter_items_list' => __( 'Filter Case study list', 'lawyer' ),
    );
    $args = array(
        'label' => __( 'Case study', 'lawyer' ),
        'description' => __( '', 'lawyer' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-media-spreadsheet',
        'supports' => array('title', 'thumbnail', 'revisions','editor', 'excerpt'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'case_study', $args );

}
add_action( 'init', 'create_case_study_cpt', 0 );


function themes_taxonomy() {
    register_taxonomy(
        'practices_categories',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'practices',             // post type name
        array(
            'hierarchical' => true,
            'label' => 'Practices category', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'practices-category',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'themes_taxonomy');

function themes_taxonomy_case_study() {
    register_taxonomy(
        'case_study_category',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'case_study',             // post type name
        array(
            'hierarchical' => true,
            'label' => 'Case study category', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'case-study-category',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            ),
            'show_ui' => true,
        )
    );
}
add_action( 'init', 'themes_taxonomy_case_study');


function themes_taxonomy_case_study_year() {
    register_taxonomy(
        'case_study_year',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'case_study',
        array(
            'hierarchical' => false,
            'label' => 'Year', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'case-study-year',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            ),
            'show_ui' => true
        )
    );
}
add_action( 'init', 'themes_taxonomy_case_study_year');

function themes_taxonomy_case_study_brand() {
    register_taxonomy(
        'case_study_brand',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'case_study',
        array(
            'hierarchical' => false,
            'label' => 'Brand', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'case-study-brand',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            ),
            'show_ui' => true
        )
    );
}
add_action( 'init', 'themes_taxonomy_case_study_brand');

function themes_taxonomy_case_study_nation() {
    register_taxonomy(
        'case_study_nation',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'case_study',
        array(
            'hierarchical' => false,
            'label' => 'Nation', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'case-study-nation',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            ),
            'show_ui' => true
        )
    );
}
add_action( 'init', 'themes_taxonomy_case_study_nation');

function themes_taxonomy_case_study_province() {
    register_taxonomy(
        'case_study_province',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'case_study',
        array(
            'hierarchical' => false,
            'label' => 'Province', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'case-study-province',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            ),
            'show_ui' => true
        )
    );
}
add_action( 'init', 'themes_taxonomy_case_study_province');