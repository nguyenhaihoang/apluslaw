// Vendors

// Css
import './src/themes/lawyer/sass/style.scss';
import 'swiper/swiper-bundle.css';

// Js
import './src/themes/lawyer/js/main.js';